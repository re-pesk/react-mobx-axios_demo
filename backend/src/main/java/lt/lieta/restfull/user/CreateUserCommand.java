package lt.lieta.restfull.user;

public final class CreateUserCommand {
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	
	public String getUserName() {
		return userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return String.format(
			"userName: %s, firstName: %s, lastName: %s, email: %s", 
			this.getUserName(), this.getFirstName(), this.getUserName(), this.email
		);
		
	}
	
}
