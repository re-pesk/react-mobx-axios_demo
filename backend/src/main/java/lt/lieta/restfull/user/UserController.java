package lt.lieta.restfull.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@Api(value="api/users")
@RequestMapping(value="/users")
public class UserController {
	
	private final UserDao userDao;
	
	@Autowired
	public UserController(UserDao userDao) {
		this.userDao = userDao;
	}
	List<User> users = new ArrayList<>();
	
	@GetMapping
	@ApiOperation(value="Get users", notes="Returns registered users")
	public List<User> getUsers(){
		//System.out.println("User list is gotten.");
		return userDao.getUsers();
	}
	
	@GetMapping(path = "/{userName}")
	@ApiOperation(value="Get user", notes="Returns user by username")
	public User getUser(
		@ApiParam(value = "User name", required = true)
		@PathVariable final String userName
	) {
		//System.out.println("Deleting user: " + userName);
		return userDao.getUser(userName);
	}


	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void createUser(@RequestBody final CreateUserCommand createUserCommand) {
		this.userDao.createUser(
			new User(
				createUserCommand.getUserName(),
				createUserCommand.getFirstName(),
				createUserCommand.getLastName(),
				createUserCommand.getEmail()
			)
		);
		//System.out.println("User created: " + createUserCommand);
	}

	@DeleteMapping(path = "/{userName}")
	@ApiOperation(value="Delete user", notes="Deletes user by username")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(
		@ApiParam(value = "User name", required = true)
		@PathVariable final String userName
	) {
		this.userDao.deleteUser(userName);
		//System.out.println("Deleting user: " + userName);
	}
}
