# React su mobx ir axios

A. Pavyzdys su "nuogu" DOM ir axios.  
B. Pavyzdys su React'o komponentu ir axios.  
C. Pavyzdys su React'o komponentu, mobx'u ir axios.  
D. Pavyzdys su React'o komponentu be statuso, mobx'u ir axios.

Ledžiant frontendą, perrašomas frontend/src/App.js failas. Visi skirtumai frontend/src/App-<raidė>.js failuose.

Pavyzdžio paleidimas

1. Jeigu dar nesate įdiegę java 8 ir node.js savo kompiuteryje, įdiekite.
2. Paleiskite backend_run.cmd ir sulaukite, kol bus įdiegti reikalingi paketai ir paleistas backendo serveris.
3. Paleiskite frontend_install.cmd ir sulaukite, kol bus įdiegti reikalingi paketai.
4. Paleiskite frontend-a_start.cmd ir sulaukite, kol pagrindinės jūsų kompiuterio naršyklės lange bus užkrautas frontendo puslapis.
5. Patikrinę programos veikimą, išjunkite frontendo serverį.
6. Pakartokite 4 ir 5 punktus su frontend-b_start.cmd, frontend-c_start.cmd ir frontend-d_start.cmd.

P.S. cmd failai veikia tik Windows'uose! 